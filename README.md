# Jenkins Install #

The Jenkins Web application ARchive (WAR) file can be started from the command line like this:

1. Download the latest stable Jenkins WAR file to an appropriate directory on your machine.]
2. Open up a terminal/command prompt window to the download directory.
3. Run the command java -jar jenkins.war.
4. Browse to http://localhost:8080 and wait until the Unlock Jenkins page appears.
5. Continue on with the Post-installation setup wizard below.

https://www.jenkins.io/doc/book/installing/war-file/

### Open Jenkins ###
1. open this url (http://[domain]:8080)
2. insert a password (password:[path]/.jenkins/secrets/initialAdminPassword)
3. follow the setup wizard
